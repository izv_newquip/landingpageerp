<div class="container section">
    <div class="row">
        <div class="col-lg-4 col-sm-4">
            <img class="img-responsive" src="assets/img/xerus_orange_hex.png" alt="">
        </div>
        <div class="col-lg-8 col-sm-8">
            <h2>Encuentra rápidamente lo que necesites</h2>
            <p>Consulta las notas compartidas o filtra y busca notas por atributos rápidamente (p.
                ej., listas con imágenes y notas con recordatorios). Encuentra lo que buscas aún más rápido y
                deja que NewQuip lo recuerde.</p>
        </div>
    </div>
</div>