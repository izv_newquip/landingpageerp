<div class="services" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>A su servicio</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 text-center">
                <i class="fa fa-4x fa-code"></i>
                <h3>Material design</h3>
                <p>Diseño minimalista con Material design</p>
            </div>
            <div class="col-lg-4 col-md-4 text-center">
                <i class="fa fa-4x fa-rocket"></i>
                <h3>Instantaneo</h3>
                <p>Más rápido que nunca</p>
            </div>
            <div class="col-lg-4 col-md-4 text-center">
                <i class="fa fa-4x fa-calendar"></i>
                <h3>Actualizado</h3>
                <p>Actualizaciones diarias</p>
            </div>
        </div>
    </div>
</div>
