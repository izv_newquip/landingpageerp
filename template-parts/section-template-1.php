<div class="container section">
    <div class="row">
        <div class="col-lg-4 col-sm-4">
            <img class="img-responsive" src="assets/img/xerus_orange_hex.png" alt="">
        </div>
        <div class="col-lg-8 col-sm-8">
            <h2>En el momento <br>y lugar adecuados</h2>
            <p>¿Necesitas acordarte de comprar algunas cosas? Crea un recordatorio basado en la ubicación. ¿Necesitas terminar una tarea
                pendiente? Crea un recordatorio basado en la hora para que nunca te olvides de nada.</p>
        </div>
    </div>
    <hr>
</div>
