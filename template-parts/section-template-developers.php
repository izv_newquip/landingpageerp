<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 14/12/16
 * Time: 12:25
 */
?>
<div class="developers" id="developers">
    <div class="container text-center ">
        <h2>Desarrolladores</h2>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <img src="assets/img/media-1.jpg" alt="" class="img-responsive img-circle center-block grayscale">
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="assets/img/media-2.jpg" alt="" class="img-responsive img-circle center-block grayscale">
            </div>
            <div class="col-lg-4 col-md-4">
                <img src="assets/img/media-3.jpg" alt="" class="img-responsive img-circle center-block grayscale">
            </div>
        </div>
    </div>
</div>
