<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container topnav">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand topnav" href="<?= $_SERVER["REQUEST_URI"] ?>">Newquip</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#home">Inicio</a></li>
                <li><a href="#services">Servicio</a></li>
<!--                <li><a href="#download">Download</a></li>-->
                <li><a href="#code">Codigo</a></li>
                <li><a href="#about">Acerca de</a></li>
            </ul>
        </div>
    </div>
</nav>
