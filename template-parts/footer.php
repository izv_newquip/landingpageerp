<?php
/**
 * Created by PhpStorm.
 * User: nono
 */
?>
<footer  id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-inline">
                    <li><a href="#home">Home</a></li>

                    <li>⋅</li>

                    <li><a href="#about">About</a></li>

                    <li>⋅</li>

                    <li><a href="#services">Services</a></li>

                    <li>⋅</li>

                    <li><a href="#contact">Contact</a></li>
                </ul>
                <p class="text-muted small">Copyright © Zaidin Vergeles 2016. Todos los derechos reservados</p>
            </div>
        </div>
    </div>
</footer>