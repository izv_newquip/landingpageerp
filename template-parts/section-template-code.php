<div class="code" id="code">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <h2>Contribuye o reporta <br> problemas en nuestro Github </h2>
            </div>
            <div class="col-lg-4 col-md-4">
                <ul class="list-inline banner-social-buttons">
                    <li><a href="https://github.com/izvdamnono/Newquip" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i><span class="network-name">Github</span></a></li>
                    <li><a href="https://bitbucket.org/izv_newquip/newquip/" class="btn btn-default btn-lg"><i class="fa fa-bitbucket"></i> <span class="network-name">Bitbucket</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>