<div class="services" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2>Contáctanos</h2>
                <!--                <h4 class="lead small">Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</h4>-->
            </div>
            <div class="col-lg-4 col-lg-offset-2 text-center">
                <i class="fa fa-4x fa-phone"></i>
                <h4>955-675-443</h4>
            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-4x fa-envelope-o"></i>
                <h4><a href="mailto:your-email@your-domain.com">izvdamdaw@gmail.com</a></h4>
            </div>
        </div>
    </div>
</div>
