<div class="container section">
    <div class="row">
        <div class="col-lg-8  col-sm-8">
            <h2>Comparte tus ideas con familiares y amigos</h2>
            <p>Comparte tu lista de la compra en NewQuip la próxima vez que vayas al supermercado.
                Ahora puedes compartir y terminar tus tareas más rápido.</p>
        </div>
        <div class="col-lg-4 col-sm-4">
            <img class="img-responsive" src="assets/img/xerus_orange_hex.png" alt="">
        </div>
    </div>
    <hr>
</div>
