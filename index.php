<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 19/11/2016
 * Time: 11:01
 */
?>
<?php require_once "template-parts/head.php" ?>
<body>
<?php require_once "template-parts/nav.php" ?>
<div class="intro-header" id="home">
    <div class="container">
        <div class="row">
            <div class="intro-message">
                <div class="col-lg-7 col-lg-offset-1 col-md-7">
                    <div id="header-left">
                        <h1>Guarda tus ideas <br>
                            estés donde estés</h1>
                        <br>
                        <a href="#download" class="btn btn-info">Probar NewQuip</a>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <div id="header-right">

                        <!--
                        <p class="lead">Inicia sesión con
                            <a class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a> o
                            <a class="btn btn-google"><i class="fa fa-google"></i> Google</a>
                        </p>
                        -->
                        <form class="form-horizontal">
                            <h3>Registrate</h3>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="password" class="form-control" name="inputPassword"
                                           placeholder="Contraseña">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="password" class="form-control" name="inputPassword2"
                                           placeholder="Repite Contraseña">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-primary">Registrarme</button>
                                </div>
                            </div>
                        </form>

                        <form class="form-horizontal">
                            <h3>Inicia sesión</h3>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="password" class="form-control" name="inputPassword"
                                           placeholder="Contraseña">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-primary">Inicia sesión</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require_once "template-parts/section-template-services.php" ?>
<?php require_once "template-parts/section-template-download.php" ?>
<?php require_once "template-parts/section-template-1.php" ?>
<?php require_once "template-parts/section-template-2.php" ?>
<?php require_once "template-parts/section-template-3.php" ?>
<?php require_once "template-parts/section-template-4.php" ?>
<?php require_once "template-parts/section-template-developers.php" ?>

<?php require_once "template-parts/section-template-contact.php" ?>
<?php require_once "template-parts/section-template-code.php" ?>

<?php require_once "template-parts/footer.php"; ?>
<?php require_once "template-parts/scripts.php"; ?>
</body>
</html>